# CS 5490/6490 Network Security #
## Final Project | Fall 2015 ##
### Music Based Authentication ###

# About #
This project is based on the idea that musical phrases can be effective passwords and easier to remember than standard typed passwords. See *Final Report* below in the Appendix. This is our implementation.

 This was written in `Python` using the `pygame` lib. Our program consists of 3 parts, `MBAuthServer`, `MBClient`, and an interface that takes inputs from a midi keyboard or computer keyboard, and translates it into a string format. The client and the server do a TLS handshake, using Ephemeral Diffie Helman. Once session keys are established, the password is encrypted and passed from the client to the server. The server stores a hashed and salted version of the password, and compares the two to ensure that the password is correct. Client is notified if their password was correct. *See appendix, figure 1*.


This provides *perfect forward secrecy*, protection from an eavesdropper, server break-in, offline-dictionary attacks and a person in the middle.

# How to run #
* This was written in [Python 2.7](https://www.python.org/downloads/release/python-2710/)
* Install the following **modules**  
    1. pygame (found here http://www.pygame.org/download.shtml)  
    2. bcrypt (I used ‘pip’ which is python’s package control: ‘pip install bcrypt’)  
* Run the server  
* Run the client  
* Click the *start* to begin input of password  
* Click the *done* to send the password
* See the [video run-through](https://www.youtube.com/watch?v=hZTgRpAYs2U)


# Contributors #
Christopher Johnson  
Terry Kingston  
Michael Anderson  
Daryl Bennett  
   
#Appendix#
  
### *Final Report* ###
[Final Report Document](http://darylbennett.net/assets/Music%20Based%20Authentication.pdf)

### *Figure 1* ###
![Keyboard Interface.png](https://bitbucket.org/repo/ndy6GK/images/1829407302-Keyboard%20Interface.png)

### *Figure 2* ###
![revised api complete.png](https://bitbucket.org/repo/ndy6GK/images/208141021-revised%20api%20complete.png)